<?php

namespace Drupal\geshi_markdown_filter\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\geshifilter\GeshiFilter;
use Drupal\geshifilter\Plugin\Filter\GeshiFilterFilter;

/**
 * Override the Geshi filter to use a custom pattern compatible with the markdown filter.
 *
 * @Filter(
 *   id = "filter_geshi_markdown",
 *   module = "geshi_markdown_filter",
 *   title = @Translation("GeSHi Markdown filter"),
 *   description = @Translation("Enables syntax highlighting of inline/block
 *     source code using the GeSHi engine, after a markdown filtering"),
 *   type = \Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   cache = FALSE,
 *   settings = {
 *     "general_tags" = {},
 *     "per_language_settings" = {}
 *   },
 *   weight = 0
 * )
 */
class GeshiMarkdownFilter extends GeshiFilterFilter {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {

    $result = new FilterProcessResult($text);

    try {
      // Load GeSHi library (if not already).
      $geshi_library = GeshiFilter::loadGeshi();
      if (!$geshi_library['loaded']) {
        throw new \Exception($geshi_library['error message']);
      }

      // Get the available tags.
      list($generic_code_tags, $language_tags, $tag_to_lang) = $this->getTags();
      if (in_array(GeshiFilter::BRACKETS_PHPBLOCK, array_filter($this->tagStyles()))) {
        $language_tags[] = 'questionmarkphp';
        $tag_to_lang['questionmarkphp'] = 'php';
      }
      $tags = array_merge($generic_code_tags, $language_tags);
      // Escape special (regular expression) characters in tags (for tags like
      // 'c++' and 'c#').
      $tags = preg_replace('#(\\+|\\#)#', '\\\\$1', $tags);

      $tags_string = implode('|', $tags);
      // Pattern for matching the prepared "<code>...</code>" stuff.
//      $pattern = '#\\[geshifilter-(' . $tags_string . ')([^\\]]*)\\](.*?)(\\[/geshifilter-\1\\])#s';
      $pattern = '#<pre[^>]*><code\sclass="(' . $tags_string . ')"([^>]*)>(.*?)(</code>)</pre>#s';
      $text = preg_replace_callback($pattern, array(
        $this,
        'replaceCallback',
      ), $text);

      // Create the object with result.
      $result = new FilterProcessResult($text);

      // Add the css file when necessary.
      if ($this->config->get('css_mode') == GeshiFilter::CSS_CLASSES_AUTOMATIC) {
        $result->setAttachments(array(
          'library' => array(
            'geshifilter/geshifilter',
          ),
        ));
      }

      // Add cache tags, so we can re-create the node when some geshifilter
      // settings change.
      $cache_tags = array('geshifilter');
      $result->addCacheTags($cache_tags);
    }
    catch (\Exception $e) {
      watchdog_exception('geshifilter', $e);
      drupal_set_message($geshi_library['error message'], 'error');
    }

    return $result;
  }
}
