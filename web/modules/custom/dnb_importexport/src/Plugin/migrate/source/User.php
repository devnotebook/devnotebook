<?php

/**
 * @file
 * User.php
 *
 * @author: Frédéric G. MARAND <fgm@osinet.fr>
 *
 * @copyright (c) 2015 Ouest Systèmes Informatiques (OSInet).
 *
 * @license General Public License version 2 or later
 */

namespace Drupal\dnb_importexport\Plugin\migrate\source;

use Symfony\Component\Yaml\Yaml;

/**
 * Drupal user source from YAML.
 *
 * @MigrateSource(
 *   id = "dnb_user"
 * )
 */
class User extends NamedInputSource {

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $ret = [
      'uid' => 'User ID',
      'uuid' => 'UUID',
      'langcode' => 'ISO 639 language code',
      'preferred_langcode' => 'ISO 639 preferred language code',
      'preferred_admin_langcode' => 'ISO 639 preferred language code for admin work',
      'name' => 'Login name',
      // 'pass'                   => 'Hashed password',
      'mail' => 'E-mail address',
      'timezone' => 'Timezone name',
      'status' => 'Status',
      'created' => 'Creation timestamp',
      'changed' => 'Modification timestamp',
      // 'access'                 => 'Last access timestamp',
      // 'login'                  => 'Last login timestamp',
      'init' => 'Initial e-mail address',
      'default_langcode' => 'ISO 639 default language code',
    ];
    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['uid']['type'] = 'integer';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function initializeIterator() {
    if (!isset($this->iterator)) {
      $input = file_get_contents($this->input);
      $parsed = Yaml::parse($input);
      unset($input);
      $users = isset($parsed['user']) ? $parsed['user'] : [];
      foreach ($users as &$user) {
        $uid = $user['uid'];
        $uid = reset($uid);
        $uid = $uid['value'];
        $user['uid'] = $uid;
      }
      $this->iterator = new \ArrayIterator($users);
    }
    else {
      $this->iterator->rewind();
    }
    return $this->iterator;
  }

}
