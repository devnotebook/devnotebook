<?php

/**
 * @file
 * Node.php
 *
 * @author: Frédéric G. MARAND <fgm@osinet.fr>
 *
 * @copyright (c) 2015 Ouest Systèmes Informatiques (OSInet).
 *
 * @license General Public License version 2 or later
 */

namespace Drupal\dnb_importexport\Plugin\migrate\source;

use Drupal\migrate\Row;
use Symfony\Component\Yaml\Yaml;

/**
 * Drupal node source from YAML.
 *
 * @MigrateSource(
 *   id = "dnb_node"
 * )
 */
class Node extends NamedInputSource {

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $ret = [
      'nid' => 'Node ID',
      'uuid' => 'UUID',
      'vid' => 'Node current revision ID',
      'type' => 'Content type',
      'langcode' => 'ISO 639 language code',
      'title' => 'Administrative title',
      'uid' => 'Author user ID',
      'status' => 'Published status',
      'created' => 'Creation timestamp',
      'changed' => 'Modification timestamp',
      'promote' => 'Promoted to front page',
      'sticky' => 'Sticky at top of lists',
      'revision_timestamp' => 'Revision timestamp',
      'revision_uuid' => 'Revision UUID',
      'revision_log' => 'Revision log',
      'default_langcode' => 'ISO 639 default language code',
      'path' => 'Optional path alias',
    ];
    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['nid']['type'] = 'integer';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function initializeIterator() {
    if (!isset($this->iterator)) {
      $input = file_get_contents($this->input);
      $parsed = Yaml::parse($input);
      unset($input);
      $nodes = isset($parsed['node']) ? $parsed['node'] : [];
      foreach ($nodes as &$node) {
        $nid = $node['nid'];
        $nid = reset($nid);
        $nid = $nid['value'];
        $node['nid'] = $nid;
      }
      $this->iterator = new \ArrayIterator($nodes);
    }
    else {
      $this->iterator->rewind();
    }
    return $this->iterator;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $this->flattenRow($row);
  }

}
