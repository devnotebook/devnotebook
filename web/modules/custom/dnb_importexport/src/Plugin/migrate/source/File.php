<?php

/**
 * @file
 * User.php.
 *
 * @author: Frédéric G. MARAND <fgm@osinet.fr>
 *
 * @copyright (c) 2015 Ouest Systèmes Informatiques (OSInet).
 *
 * @license General Public License version 2 or later
 */

namespace Drupal\dnb_importexport\Plugin\migrate\source;

use Drupal\migrate\Entity\MigrationInterface;
use Drupal\migrate\Row;
use Symfony\Component\Yaml\Yaml;

/**
 * Drupal user source from YAML.
 *
 * @MigrateSource(
 *   id = "dnb_file"
 * )
 */
class File extends NamedInputSource {

  /**
   * File path.
   *
   * @var string
   */
  protected $filePath;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, MigrationInterface $migration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $file_path = \Drupal::config('ftvisi_staging.settings')->get('settings.path');
    $file_path .= '/public';
    $real_file_path = realpath($file_path);
    if (empty($real_file_path)) {
      throw new \InvalidArgumentException(t('Invalid path: @path', ['@path' => $file_path]));
    }
    $this->filePath = $real_file_path;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $ret = [
      'fid' => 'File ID',
      'uuid' => 'UUID',
      'langcode' => 'ISO 639 language code',
      'uid' => 'File creator ID',
      'filename' => 'File name without a directory',
      'uri' => 'Access path to the file using a stream wrapper',
      'filemime' => 'MIME type for the file',
      'filesize' => 'Size of the file in bytes',
      'status' => 'Status: temporary or permanent',
      'created' => 'Creation timestamp',
      'changed' => 'Modification timestamp',
    ];
    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['fid']['type'] = 'integer';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function initializeIterator() {
    if (!isset($this->iterator)) {
      $input = file_get_contents($this->input);
      $parsed = Yaml::parse($input);
      unset($input);
      $files = isset($parsed['file']) ? $parsed['file'] : [];
      foreach ($files as &$file) {
        $fid = $file['fid'];
        $fid = reset($fid);
        $fid = $fid['value'];
        $file['fid'] = $fid;
      }
      $this->iterator = new \ArrayIterator($files);
    }
    else {
      $this->iterator->rewind();
    }
    return $this->iterator;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $this->flattenRow($row);
    $uri = $row->getSource()['uri'];
    $row->setSourceProperty('filepath', $this->filePath . '/' . str_replace('public://', '', $uri));
  }

  /**
   * {@inheritdoc}
   */
  public function next() {
    parent::next();
  }

}
