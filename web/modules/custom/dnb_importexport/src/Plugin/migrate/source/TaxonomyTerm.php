<?php

/**
 * @file
 * Taxonomy.php.
 *
 * @author: Frédéric G. MARAND <fgm@osinet.fr>
 *
 * @copyright (c) 2015 Ouest Systèmes Informatiques (OSInet).
 *
 * @license General Public License version 2 or later
 */

namespace Drupal\dnb_importexport\Plugin\migrate\source;

use Drupal\migrate\Row;
use Symfony\Component\Yaml\Yaml;

/**
 * Drupal Taxonomy source from YAML.
 *
 * @MigrateSource(
 *   id = "dnb_taxonomy_term"
 * )
 */
class TaxonomyTerm extends NamedInputSource {

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $ret = [
      'tid' => 'Taxonomy term ID',
      'uuid' => 'UUID',
      'vid' => 'Vocabulary ID',
      'langcode' => 'ISO 639 language code',
      'name' => 'Term name',
      'description' => 'Description',
      'weight' => 'Poids',
      'parent' => 'Parent term ID',
      'changed' => 'Modification timestamp',
      'default_langcode' => 'ISO 639 default language code',
      'path' => 'Optional path alias',
    ];
    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return ['tid' => ['type' => 'integer']];
  }

  /**
   * {@inheritdoc}
   */
  public function initializeIterator() {
    if (!isset($this->iterator)) {
      $input = file_get_contents($this->input);
      $parsed = Yaml::parse($input);
      unset($input);
      $terms = isset($parsed['taxonomy_term']) ? $parsed['taxonomy_term'] : [];
      foreach ($terms as &$term) {
        $tid = $term['tid'];
        $tid = reset($tid);
        $tid = $tid['value'];
        $term['tid'] = $tid;
      }
      $this->iterator = new \ArrayIterator($terms);
    }
    else {
      $this->iterator->rewind();
    }
    return $this->iterator;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $this->flattenRow($row);
  }

}
