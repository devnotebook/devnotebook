<?php

/**
 * @file
 * User.php.
 *
 * @author: Frédéric G. MARAND <fgm@osinet.fr>
 *
 * @copyright (c) 2015 Ouest Systèmes Informatiques (OSInet).
 *
 * @license General Public License version 2 or later
 */

namespace Drupal\dnb_importexport\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;
use Drupal\migrate\Entity\MigrationInterface;
use Drupal\migrate\Row;
use Symfony\Component\Yaml\Yaml;

/**
 * Drupal source using an named input property to find its data.
 *
 * This allows migration users to use varying inputs every time by accessing
 * the migration source and setting the input prior to running import().
 */
abstract class NamedInputSource extends SourcePluginBase {

  /**
   * The input file name, or NULL for php://stdin.
   *
   * @var string
   */
  protected $input;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->iterator = NULL;
  }

  /**
   * Set the name of an input source.
   *
   * @param string $input
   *   The name of an input source, so that it may vary at runtime.
   */
  public function setInput($input = NULL) {
    $this->input = $input;
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    $current = $this->iterator->current();
    $ret = json_encode($current, JSON_PRETTY_PRINT);
    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  protected function flattenRow(Row $row) {
    $source = $row->getSource();
    foreach ($source as $key => &$item_list) {
      if (is_scalar($item_list)) {
        continue;
      }
      if (count($item_list) > 1) {
        $item = $item_list;
      }
      else {
        $item = reset($item_list);
      }

      if (is_scalar($item) || (count($item) != 1 && !isset($item['width']) && !isset($item['pid']))) {
        $value = $item;
      }
      elseif (isset($item['value'])) {
        $value = $item['value'];
      }
      // Handle bundle['target_id']
      // Exclude image field to keep metadata (alt / title)
      elseif (isset($item['target_id']) && !isset($item['alt']) && !isset($item['title'])) {
        $value = $item['target_id'];
      }
      elseif (isset($item['pid'])) {
        $value = $item['alias'];
      }
      else {
        $value = $item;
      }

      if (empty($item)) {
        $value = NULL;
      }

      $row->setSourceProperty($key, $value);
    }
  }

}
