<?php

/**
 * @file
 * Media.php
 */

namespace Drupal\dnb_importexport\Plugin\migrate\source;

use Drupal\migrate\Row;
use Symfony\Component\Yaml\Yaml;

/**
 * Drupal node source from YAML.
 *
 * @MigrateSource(
 *   id = "dnb_media"
 * )
 */
class Media extends NamedInputSource {

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $ret = [
      // Media baseFieldDefinitions
      'mid' => 'Media ID',
      'uuid' => 'UUID',
      'vid' => 'Revision ID',
      'bundle' => 'Bundle',
      'langcode' => 'Language code',
      'name' => 'Media name',
      'thumbnail' => 'Thumbnail',
      'uid' => 'Publisher ID',
      'status' => 'Publishing status',
      'created' => 'Created',
      'changed' => 'Changed',
      'type' => 'Type',
      'revision_timestamp' => 'Revision timestamp',
      'revision_uid' => 'Revision publisher ID',
      'revision_log' => 'Revision Log',
      'default_langcode' => 'Default translation',
    ];
    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['mid']['type'] = 'integer';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function initializeIterator() {
    if (!isset($this->iterator)) {
      $input = file_get_contents($this->input);
      $parsed = Yaml::parse($input);
      unset($input);
      $medias = isset($parsed['media']) ? $parsed['media'] : [];
      foreach ($medias as &$media) {
        $mid = $media['mid'];
        $mid = reset($mid);
        $mid = $mid['value'];
        $media['mid'] = $mid;
      }
      $this->iterator = new \ArrayIterator($medias);
    }
    else {
      $this->iterator->rewind();
    }
    return $this->iterator;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $this->flattenRow($row);
  }

}
