<?php

/**
 * @file
 * MenuLink.php
 *
 * @author: Frédéric G. MARAND <fgm@osinet.fr>
 *
 * @copyright (c) 2015 Ouest Systèmes Informatiques (OSInet).
 *
 * @license General Public License version 2 or later
 */

namespace Drupal\dnb_importexport\Plugin\migrate\source;

use Symfony\Component\Yaml\Yaml;

/**
 * Drupal menu_link_content source from YAML.
 *
 * @MigrateSource(
 *   id = "dnb_menu_link"
 * )
 */
class MenuLink extends NamedInputSource {

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $ret = [
      'id' => 'id',
      'uuid' => 'uuid',
      'bundle' => 'bundle',
      'title' => 'title',
      'description' => 'description',
      'menu_name' => 'menu_name',
      'changed' => 'changed',
      'default_langcode' => 'default_langcode',
    ];
    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['id']['type'] = 'integer';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function initializeIterator() {
    if (!isset($this->iterator)) {
      $input = file_get_contents($this->input);
      $parsed = Yaml::parse($input);
      unset($input);
      $menu_links = isset($parsed['menu_link_content']) ? $parsed['menu_link_content'] : [];
      foreach ($menu_links as &$menu_link) {
        $id = $menu_link['id'];
        $id = reset($id);
        $id = $id['value'];
        $menu_link['id'] = $id;

        $bundle = $menu_link['bundle'];
        $bundle = reset($bundle);
        $bundle = $bundle['value'];
        $menu_link['bundle'] = $bundle;
      }
      $this->iterator = new \ArrayIterator($menu_links);
    }
    else {
      $this->iterator->rewind();
    }
    return $this->iterator;
  }

}
