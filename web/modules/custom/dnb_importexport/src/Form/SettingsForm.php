<?php

namespace Drupal\dnb_importexport\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Formulaire d'administration d'import/export de contenus.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dnb_importexport_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'dnb_importexport.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('dnb_importexport.settings');

    $form['general'] = [
      '#type' => 'details',
      '#title' => t('General settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#open' => TRUE,
    ];

    $form['general']['path'] = [
      '#type' => 'textfield',
      '#title' => t('Import dir path'),
      '#description' => t('Enter a path relative to the Drupal root directory.'),
      '#default_value' => $config->get('path'),
      '#placeholder' => '../import',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $path = $form_state->getValue('path');
    $real = realpath(DRUPAL_ROOT . '/' . $path);
    if (empty($real) || !is_dir($real) || !is_readable($real)) {
      $form_state->setErrorByName('path', t('@path is not a readable directory.', ['@path' => $path]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('dnb_importexport.settings')
        ->set('path', $form_state->getValue('path', ''))
        ->save();

    parent::submitForm($form, $form_state);
  }

}
