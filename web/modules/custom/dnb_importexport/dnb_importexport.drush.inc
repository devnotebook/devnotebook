<?php

use Drupal\Component\Serialization\Yaml;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\MigrateMessage;
use Drupal\migrate_plus\Entity\Migration;

/**
 * Implements hook_drush_command().
 */
function dnb_importexport_drush_command() {
  $items['content-export'] = [
    'description' => t('Export site contents.'),
    'arguments' => [],
    'required-arguments' => 0,
    'aliases' => ['ce'],
    'options' => [],
    'drupal dependencies' => [],
  ];

  $items['content-import'] = [
    'description' => t('Import site contents.'),
    'arguments' => [
      'name' => t('The machine name for the migration'),
      'input' => t('File containing data to import.'),
    ],
    'required-arguments' => 2,
    'aliases' => ['ci'],
    'options' => [
      'rollback' => t('Rollback the migration.'),
    ],
    'drupal dependencies' => ['migrate', 'dnb_importexport'],
  ];

  return $items;
}

/**
 * Exporte toutes les entités dont le type est en argument.
 *
 * @param string $entity_type Le type d'entité à exporter
 */
function drush_dnb_importexport_content_export($entity_type = null) {

  $entity_service = \Drupal::service('dnb_api.service.entity');
  $entity_type_list = $entity_service->getEntiyTypes();

  if (!empty($entity_type)) {
    $entity_type_list = [$entity_type => $entity_type_list[$entity_type]];
  }

  /** @var \Symfony\Component\Serializer\Serializer $serializer */
  $serializer = \Drupal::service('serializer');

  $path = \Drupal::config('dnb_importexport.settings')->get('path');
  $real = realpath(DRUPAL_ROOT . '/' . $path);
  $alias_service = \Drupal::service('path.alias_storage');

  /** @var \Drupal\Core\Entity\EntityType $entity_info */
  foreach ($entity_type_list as $entity_type => $entity_info) {
    if ($entity_info->hasKey('bundle')) {
      $entities = [];
      $bundles = array_keys(entity_get_bundles($entity_type));
      foreach ($bundles as $bundle) {
        $entities[$entity_type] = entity_load_multiple_by_properties($entity_type, [$entity_info->getKey('bundle') => $bundle]);

        if ($entity_type == 'node') {

          drush_log(dt('Node export.', []), 'info');
          foreach ($entities['node'] as $nid => $node) {
            $alias = $alias_service->load(array('source' => "/node/$nid"));
            if ($alias) {
              $node->path = $alias;
            }
          }
        }
        elseif ($entity_type == 'taxonomy_term') {

          drush_log(dt('Taxonomy term export.', []), 'info');
          foreach ($entities['taxonomy_term'] as $tid => $term) {
            $alias = $alias_service->load(array('source' => "/taxonomy_term/$tid"));
            if ($alias) {
              $term->path = $alias;
            }
          }
        }
        elseif ($entity_type == 'user') {

          drush_log(dt('User export.', []), 'info');
          foreach ($entities['user'] as $uid => $user) {
            $alias = $alias_service->load(array('source' => "/user/$uid"));
            if ($alias) {
              $user->path = $alias;
            }
          }
        }

        $json = $serializer->serialize($entities, 'json');
        $hash = json_decode($json, TRUE);

        if (!file_exists($real . '/' . $entity_type)) {
          mkdir($real . '/' . $entity_type, 0777, TRUE);
        }
        $file_path = $real . '/' . $entity_type . '/' . $bundle . '.' . Yaml::getFileExtension();

        drush_log(dt('Exported to @file_path.', ['@file_path' => $file_path]), 'info');
        file_put_contents($file_path, Yaml::encode($hash));
      }
    }
    else {
      $entities = [];

      $entities[$entity_type] = entity_load_multiple($entity_type);

      // These user are created during install,
      // we do not want them to be overridden.
      unset($entities['user'][0], $entities['user'][1]);

      $json = $serializer->serialize($entities, 'json');
      $hash = json_decode($json, TRUE);

      if (!file_exists($real . '/' . $entity_type)) {
        mkdir($real . '/' . $entity_type, 0777, TRUE);
      }

      file_put_contents($real . '/' . $entity_type . '/' . $entity_type . '.' . Yaml::getFileExtension(), Yaml::encode($hash));
    }
  }
}

/**
 * Importe toutes les entités d'un certain type, à partir du fichier en argument.
 *
 * @param string $migration_id Type d'entité à importer.
 * @param string $input_file Nom et chemin du fichier d'import à utiliser
 */
function drush_dnb_importexport_content_import($migration_id, $input_file) {

  $migration = \Drupal::service('plugin.manager.migration')->createInstance($migration_id);

  if (!empty($migration)) {

    /** @var \Drupal\dnb_importexport\Plugin\migrate\source\NamedInputSource $source */
    $source = $migration->getSourcePlugin();
    $source->setInput($input_file);

    $executable = new MigrateExecutable($migration, new MigrateMessage());

    $rollback = drush_get_option('rollback', FALSE);
    if (!$rollback) {
      $sts = $executable->import();
    }
    else {
      $sts = $executable->rollback();
    }

    drush_log(dt('Traitement de la migration @name (@id) à partir de la source @input', [
      '@name' => $migration_id,
      '@id' => $migration->id(),
      '@input' => $input_file,
        ]), 'ok');
  }
  else {
    drush_log(dt('Migration @name introuvable.', ['@name' => $migration_id]), 'error');
    return;
  }
}
