<?php

namespace Drupal\hostname_replace_filter\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Filter pour remplacer un nom de domaine par un autre dans les champs texte.
 *
 * @Filter(
 *   id = "filter_hostname_replace",
 *   module = "hostname_replace_filter",
 *   title = @Translation("Hostname replace filter"),
 *   description = @Translation("Replace all occurrences of a defined hostname by another one"),
 *   type = \Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   cache = FALSE,
 *   settings = {
 *     "hostname_from" =  "",
 *     "hostname_to" = "",
 *     "disabled_from" = 1
 *   },
 *   weight = 0
 * )
 */
class HostnameReplaceFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $form['hostname_from'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hostname from'),
      '#description' => $this->t('The hostname which has to be replaced'),
      '#default_value' => $this->settings['hostname_from'],
    ];
    $form['hostname_to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hostname to'),
      '#description' => $this->t('The hostname by with replace'),
      '#default_value' => $this->settings['hostname_to'],
    ];
    $form['disabled_from'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable filtering if the current site is <strong>@hostname</strong>', ['@hostname' => !empty($this->settings['hostname_from']) ? $this->settings['hostname_from'] : '"from hostname"']),
      '#description' => $this->t('If checked, this filter will be ignored if the website hostname is the one in the <strong>Hostname from</strong> field.'),
      '#default_value' => $this->settings['disabled_from'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    if (!empty($text) && !empty($this->settings['hostname_from']) && !empty($this->settings['hostname_to'])) {

      $current_hostname = \Drupal::requestStack()->getCurrentRequest()->getHost();
      if (($current_hostname != $this->settings['hostname_from'] || !$this->settings['disabled_from']) && strpos($text, $this->settings['hostname_from']) !== FALSE) {
        $text = str_replace($this->settings['hostname_from'], $this->settings['hostname_to'], $text);
      }
    }

    return new FilterProcessResult($text);
  }

}
