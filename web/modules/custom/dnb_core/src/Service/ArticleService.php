<?php

namespace Drupal\dnb_core\Service;

use \InvalidArgumentException;
use \stdClass;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Classe ArticleService.
 *
 * Propose des fonctions utiles pour la gestion des articles.
 *
 * @package Drupal\dnb_core\Service
 */
class ArticleService {

  /**
   * Gestionnaire d'entités.
   *
   * @var EntityTypeManager
   */
  private $entityTypeManager;

  /**
   * Gestionnaire d'entités.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $nodeStorage;

  /**
   * Crée une nouvelle instance.
   *
   * @param EntityTypeManager $entity_type_manager
   *   Gestionnaire d'entités.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->nodeStorage = $this->entityTypeManager->getStorage('node');
  }

  /**
   * Retourne tous les articles publiés dans la catégorie en argument.
   *
   * @param Term $term
   *   Catégorie des articles recherchés.
   * @param string $return_type
   *   Format des résultats attendu :
   *    - object : les résultats seront listés sous forme de noeuds chargés
   *    - id : les résultat seront listés sous forme de nid
   *    - nb : seul le nombre de résultat sera retourné
   *    Par défaut, le format 'object' est choisi.
   * @param bool $exact_term
   *   Si les articles doivent appartenir exactement à la catégorie ou aussi
   *   à ses filles.
   *
   * @return Node[]|int[]|int
   *   La liste des articles trouvés, ou leur id, ou le nombre d'articles
   *
   * @throws InvalidArgumentException
   *   Si le type de retour n'est pas géré.
   */
  public function getByCategory(Term $term, $return_type = 'object', $exact_term = TRUE) {

    if (!in_array($return_type, ['object', 'id', 'nb'])) {
      throw new InvalidArgumentException();
    }

    $query = $this->getByCategoryQuery($term->id());

    if ($return_type == 'nb') {
      $result = $query->count()->execute();
    }
    else {
      $result = $query->execute();
    }

    // Si on veut aussi les articles des catégories filles.
    if (!$exact_term) {
      $taxonomy_service = \Drupal::service('dnb_api.service.taxonomy');
      $children_tids = $taxonomy_service->getChildrenTids($term->id());

      if (!empty($children_tids)) {
        $query = $this->getByCategoryQuery($children_tids);
        if ($return_type == 'nb') {
          $result += $query->count()->execute();
        }
        else {
          $result += $query->execute();
        }
      }
    }

    if ($return_type == 'object') {
      $result = Node::loadMultiple($result);
    }

    return $result;
  }

  /**
   * Retourne la navigation associée à l'article en argument.
   *
   * Elle représente la liste des articles liés, dans l'ordre.
   *
   * @param Node $article
   *   Article dont on veut la navigation.
   *
   * @return Node[]
   *   La liste des articles constituant la navigation
   */
  public function getNavigation(Node $article) {

    $navigation = [];

    if (!$article->get('field_article_parent')->isEmpty()) {
      $parent_id = $article->get('field_article_parent')->getString();
    }
    else {
      $parent_id = $article->id();
    }

    // Récupération des articles fils.
    $query = $this->nodeStorage->getQuery()
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('type', 'article')
      ->condition('field_article_parent', $parent_id)
      ->sort('created', 'ASC');
    $result = $query->execute();

    if (!empty($result)) {
      array_unshift($result, $parent_id);
      $navigation = Node::loadMultiple($result);
    }

    return $navigation;
  }

  /**
   * Retourne la requête listant les articles associés au terme en argument.
   *
   * @param int|array $term_id
   *   ID ou liste d'ID des catégories des articles recherchés.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   La requête pour lister les nid des articles trouvés
   */
  private function getByCategoryQuery($term_id) {

    $query = $this->nodeStorage->getQuery()
      ->condition('status', NodeInterface::PUBLISHED)
      ->sort('created', 'DESC');

    if (is_array($term_id)) {
      $query->condition('field_categorie', $term_id, 'IN');
    }
    else {
      $query->condition('field_categorie', $term_id);
    }

    return $query;
  }

  /**
   * Retourne la distribution des articles parmi les termes en argument.
   *
   * @param array $term_tree_list
   *   Liste d'arbres de catégories.
   * @param \stdClass|null $distribution
   *   Distribution à compléter (utilisée pour la récursivité).
   *
   * @return \stdClass
   *   Objet avec deux propriétés :
   *    - int sum : nombre d'articles total parmi les termes en argument
   *    - array items : tableau associatif avec "term" et "nb" pour clés
   *    - array order : clés du tableau items, triées par ordre de poids
   *
   *   Remarque : la somme compte une seule un article catégorisé dans
   *   plusieurs des termes en arguments.
   *
   * @throws InvalidArgumentException
   *   Si le type de retour n'est pas géré.
   */
  public function getDistribution(array $term_tree_list, $distribution = NULL) {

    if (empty($distribution)) {
      $distribution = new stdClass();
      $distribution->items = [];
      $distribution->sum = 0;
      $distribution->order = [];
    }

    $all_nid_list = [];

    if (!empty($term_tree_list)) {

      // Si la racine de l'arbre a été simulée, elle n'a pas de terme réel.
      if (current($term_tree_list)->term == NULL) {
        $distribution = $this->getDistribution(current($term_tree_list)->children, $distribution);
      }
      else {
        foreach ($term_tree_list as $term_item) {

          $term = Term::load($term_item->term->tid);
          $nid_list = $this->getByCategory($term, 'id', FALSE);
          $distribution->items[] = [
            'term' => $term,
            'nb' => count($nid_list),
            'weight' => $this->calcDistributionWeight($term_item),
          ];

          // Calcul de la distribution des enfants.
          if (!empty($term_item->children)) {
            $distribution = $this->getDistribution($term_item->children, $distribution);
          }

          $all_nid_list = array_merge($nid_list, $all_nid_list);
        }
        $distribution->sum = count($all_nid_list);

        $sorted_list = $distribution->items;
        uasort($sorted_list, function ($item1, $item2) {
          $nb1 = $item1['nb'] * (1 + $item1['weight']);
          $nb2 = $item2['nb'] * (1 + $item1['weight']);
          return ($nb1 < $nb2) ? -1 : 1;
        });
        $distribution->order = array_keys($sorted_list);
      }
    }

    return $distribution;
  }

  /**
   * Retourne le poids à attribuer au terme de taxonomie en argument.
   *
   * Le poids va de 0 à 1.
   * Le terme gagne 0.1 de poids pour chacun de ses ancêtres, et un bonus de 0.5
   * s'il n'a pas de fils.
   *
   * @param \stdClass $term_item
   *   Terme simplifié dont on veut le poids.
   *
   * @return float
   *   Le poids, entre 0 et 1
   */
  private function calcDistributionWeight(stdClass $term_item) {

    if (empty($term_item->children)) {
      $weight = 0.5;
    }
    else {
      $weight = count($term_item->ancestor_id_list) * 0.1;
    }

    return $weight;
  }

}
