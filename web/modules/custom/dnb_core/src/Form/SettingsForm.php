<?php

namespace Drupal\dnb_core\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Gère le formulaire de configuration pour le site Devnotebook.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dnb_core_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'dnb_core.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('dnb_core.settings');

    $form['site_information'] = [
      '#type' => 'fieldset',
      '#title' => t('Informations'),
    ];

    $form['site_information']['dnb_core_site_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Version de l'application"),
      '#default_value' => $config->get('dnb_core_site_version'),
      '#attributes' => ['disabled' => 'disabled'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('dnb_core.settings')
      ->set('dnb_core_site_version', $form_state->getValue('dnb_core_site_version'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
