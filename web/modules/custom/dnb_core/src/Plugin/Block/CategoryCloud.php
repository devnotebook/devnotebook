<?php

namespace Drupal\dnb_core\Plugin\Block;

use \Drupal;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\dnb_api\Service\TaxonomyService;
use Drupal\dnb_core\Service\ArticleService;
use Drupal\dnb_core\Service\ConstantService;

/**
 * @Block(
 *   id = "dnb_categorycloud",
 *   admin_label = @Translation("Category cloud"),
 *   category = @Translation("Devnotebook")
 * )
 */
class CategoryCloud extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    /** @var TaxonomyService $taxonomy_service */
    $taxonomy_service = Drupal::service('dnb_api.service.taxonomy');
    /** @var ArticleService $article_service */
    $article_service = Drupal::service('dnb_core.service.article');

    // Récupération des catégories à afficher
    $term_tree = $taxonomy_service->getVocabularyTermTree(ConstantService::VOCAB_NAME_CATEGORIES);
    $distribution = $article_service->getDistribution([$term_tree]);
    $nb_categ = count($distribution->items);

    // Répartition en 6 tranches
    /** @var \stdClass $item */
    foreach ($distribution->order as $index => $item_key) {

      switch (TRUE) {
        case $index < 0.1 * $nb_categ:
          $distribution->items[$item_key]['weight'] = 0;
          break;

        case ($index >= 0.1 * $nb_categ) && ($index < 0.3 * $nb_categ):
          $distribution->items[$item_key]['weight'] = 1;
          break;

        case ($index >= 0.3 * $nb_categ) && ($index < 0.5 * $nb_categ):
          $distribution->items[$item_key]['weight'] = 2;
          break;

        case ($index >= 0.5 * $nb_categ) && ($index < 0.7 * $nb_categ):
          $distribution->items[$item_key]['weight'] = 3;
          break;

        case ($index >= 0.7 * $nb_categ) && ($index < 0.9 * $nb_categ):
          $distribution->items[$item_key]['weight'] = 4;
          break;

        case $index > 0.9 * $nb_categ:
          $distribution->items[$item_key]['weight'] = 5;
          break;
      }
    }
    shuffle($distribution->items);

    $build = [
      '#cache' => [
        'tags' => ['taxonomy_term_list', 'node_list'],
        'max-age' => Cache::PERMANENT,
      ],
      'menu' => [
        '#theme' => 'category_cloud',
        '#distribution' => $distribution,
        '#variables' => [],
      ],
    ];

    return $build;
  }

}
