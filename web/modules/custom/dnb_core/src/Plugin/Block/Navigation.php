<?php

namespace Drupal\dnb_core\Plugin\Block;

use \Drupal;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\dnb_api\Service\TaxonomyService;
use Drupal\dnb_core\Service\ConstantService;

/**
 * Bloc pour afficher un arbre de catégories.
 *
 * @Block(
 *   id = "dnb_navigation",
 *   admin_label = @Translation("Navigation"),
 *   category = @Translation("Devnotebook")
 * )
 */
class Navigation extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    /** @var TaxonomyService $taxonomy_service */
    $taxonomy_service = Drupal::service('dnb_api.service.taxonomy');
    $term_tree = NULL;

    // Récupération de la catégorie courante
    $current_category = $taxonomy_service->getCurrentCategory([ConstantService::VOCAB_NAME_CATEGORIES]);
    $current_category_tid = !empty($current_category) ? $current_category->tid : 0;

    if (!empty($current_category_tid)) {
      // Récupération des catégories à afficher
      $term_tree = $taxonomy_service->getVocabularyTermTree(ConstantService::VOCAB_NAME_CATEGORIES);
      $term_tree = $taxonomy_service->getParentSubTree($term_tree, $current_category_tid);
    }

    return [
      '#cache' => [
        'tags' => ['taxonomy_term_list'],
        'max-age' => Cache::PERMANENT,
      ],
      'current_category' => $current_category,
      'tree' => $term_tree,
    ];
  }

}
