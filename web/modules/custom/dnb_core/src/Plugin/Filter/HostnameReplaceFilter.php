<?php

namespace Drupal\dnb_core\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Filter pour remplacer un nom de domaine par un autre dans les champs texte.
 *
 * @Filter(
 *   id = "filter_hostname_replace",
 *   module = "dnb_core",
 *   title = @Translation("Hostname replace filter"),
 *   description = @Translation("Replace all occurrences of a defined hostname by another one"),
 *   type = \Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   cache = FALSE,
 *   settings = {
 *     "hostname_from" =  "",
 *     "hostname_to" = ""
 *   },
 *   weight = 0
 * )
 */
class HostnameReplaceFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $form['hostname_from'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hostname from'),
      '#description' => $this->t('The hostname which has to be replaced'),
      '#default_value' => $this->settings['hostname_from'],
    ];
    $form['hostname_to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hostname to'),
      '#description' => $this->t('The hostname by with replace'),
      '#default_value' => $this->settings['hostname_to'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    if (!empty($text) && !empty($this->settings['hostname_from']) && !empty($this->settings['hostname_to'])) {

      if (strpos($text, $this->settings['hostname_from']) !== FALSE) {
        $text = str_replace($this->settings['hostname_from'], $this->settings['hostname_to'], $text);
      }
    }

    return new FilterProcessResult($text);
  }

}
