<?php

/**
 * @file
 * Global hooks for the Devnotebook website.
 */

use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\dnb_api\Service\TaxonomyService;
use Drupal\dnb_core\Service\ArticleService;
use Drupal\dnb_core\Service\ConstantService;
use Drupal\taxonomy\Entity\Term;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\Plugin\views\query\Sql;
use Drupal\views\ViewExecutable;

/**
 * Implements hook_help().
 */
function dnb_core_help($route_name, RouteMatchInterface $route_match) {

  $output = NULL;
  if ($route_name == 'help.page.dnb_core') {
    $output = '<h3>' . t('About') . '</h3>';
    $output .= '<p>' . t('Gère les fonctionnalités principales du site.') . '</p>';
  }
  return $output;
}

/**
 * Implements hook_entity_view().
 */
function dnb_core_node_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {

  if ($entity->bundle() == 'article') {

    if (\Drupal::currentUser()->hasPermission('content edit')) {
      $build['edit_link'] = [
        '#type' => 'markup',
        '#markup' => sprintf(
          '<a href="%s" title="%s"><span class="icon icon-pencil" aria-hidden="true"></span></a>',
          Url::fromRoute('entity.node.edit_form', ['node' => $entity->id()])
            ->toString(),
          t('Edit')
        ),
      ];
    }
  }
}

/**
 * Implements hook_entity_view().
 */
function dnb_core_taxonomy_term_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {

  if ($entity->bundle() == 'categorie') {
    /** @var ArticleService $article_service */
    $article_service = \Drupal::service('dnb_core.service.article');
    $entity->dnb_data = [
      'nb_articles' => $article_service->getByCategory($entity, 'nb', FALSE),
    ];
  }
}

/**
 * Implements hook_views_query_alter().
 */
function dnb_core_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {

  // Modification des résultats pour la vue Terme de taxonomie
  if ($view->id() == 'taxonomy_term') {

    /*
     * On parcourt les groupes de conditions dans le champ where de la requête.
     * Pour chaque condition à l'intérieur, on cherche celle qui spécifie la
     * catégorie à laquelle doivent appartenir les contenus qu'affiche la vue.
     * On remplace la condition avec catégorie exacte par une liste d'id de
     * catégorie (catégorie courante + catégories filles).
     */

    /** @var Sql $query */
    foreach ($query->where as &$where_clause) {
      if (!empty($where_clause['conditions'])) {

        foreach ($where_clause as &$condition_group) {

          if (is_array($condition_group)) {
            foreach ($condition_group as &$condition) {
              if (is_array($condition['value'])
                && array_key_exists(':taxonomy_index_tid', $condition['value'])
                && $condition['field'] == 'taxonomy_index.tid = :taxonomy_index_tid') {

                $current_category_id = $condition['value'][':taxonomy_index_tid'];
                /** @var TaxonomyService $taxonomy_service */
                $taxonomy_service = \Drupal::service('dnb_api.service.taxonomy');
                $descendant_id_list = $taxonomy_service->getChildrenTids($current_category_id);

                if (!empty($descendant_id_list)) {
                  $condition['field'] = 'taxonomy_index.tid IN (:taxonomy_index_tid[])';
                  unset($condition['value'][':taxonomy_index_tid']);
                  $condition['value'][':taxonomy_index_tid[]'] = array_merge($descendant_id_list, [$current_category_id]);
                }
              }
            }
          }
        }
      }
    }
  }
}

/**
 * Implements hook_block_view_alter().
 */
function dnb_core_block_view_dnb_taxonomy_menu_alter(array &$build, BlockPluginInterface $block) {

  // Ajout d'un pre_render
  $build['#pre_render'][] = '_dnb_core_block_taxonomy_menu_prerender';
}

/**
 * Implements hook_theme().
 */
function dnb_core_theme() {
  return [
    'category_cloud' => [
      'template' => 'category_cloud',
      'variables' => [
        'distribution' => [],
        'variables' => [],
      ],
    ],
  ];
}

/**
 * Implements hook_toolbar().
 */
function dnb_core_toolbar() {

  $items = [];
  $add_article_url_params = [];

  // Récupération de la catégorie courante
  /** @var TaxonomyService $taxonomy_service */
  $taxonomy_service = Drupal::service('dnb_api.service.taxonomy');
  $current_category = $taxonomy_service->getCurrentCategory([ConstantService::VOCAB_NAME_CATEGORIES], TRUE);
  if (!empty($current_category)) {
    $add_article_url_params['query'] = ['category' => $current_category->id()];
  }

  $links = [
    // Lien d'ajout d'article
    [
      '#type' => 'link',
      '#title' => t('Add an article'),
      '#url' => Url::fromRoute('node.add', [
        'node_type' => 'article',
      ], $add_article_url_params),
      '#options' => [
        'attributes' => [
          'class' => ['toolbar-icon', 'toolbar-icon-system-admin-content'],
        ],
      ],
      '#prefix' => '<li class="menu-item menu-item--expanded">',
      '#suffix' => '</li></ul>',
    ],
    // Lien d'ajout d'image
    [
      '#type' => 'link',
      '#title' => t('Add an image'),
      '#url' => Url::fromRoute('entity.media.add_form', [
        'media_bundle' => 'image',
      ]),
      '#options' => [
        'attributes' => [
          'class' => ['toolbar-icon', 'toolbar-icon-system-themes-page'],
        ],
      ],
      '#prefix' => '<ul class="toolbar-menu"><li class="menu-item menu-item--expanded">',
      '#suffix' => '</li>',
    ],
    // Lien vers la liste des catégories
    [
      '#type' => 'link',
      '#title' => t('Catégories'),
      '#url' => Url::fromRoute('entity.taxonomy_vocabulary.overview_form', [
        'taxonomy_vocabulary' => ConstantService::VOCAB_NAME_CATEGORIES,
      ]),
      '#options' => [
        'attributes' => [
          'class' => ['toolbar-icon', 'toolbar-icon-system-admin-structure'],
        ],
      ],
      '#prefix' => '<li class="menu-item menu-item--expanded">',
      '#suffix' => '</li></ul>',
    ],
  ];

  $items['devnotebook'] = [
    '#type' => 'toolbar_item',
    '#weight' => 10,
    'tab' => [
      '#type' => 'link',
      '#title' => t('Devnotebook'),
      '#url' => Url::fromRoute('devnotebook.settings'),
      '#attributes' => [
        'title' => t('Devnotebook'),
        'class' => ['toolbar-icon', 'toolbar-icon-devel'],
      ],
    ],
    'tray' => [
      '#heading' => t('Devnotebook'),
      'devnotebook' => $links,
    ],
  ];

  return $items;
}

/**
 * Implements hook_preprocess_node().
 */
function dnb_core_preprocess_node(&$variables) {

  if ($variables['node']->getType() == 'article') {

    // Type d'article
    $article_type = '';
    if (!$variables['node']->get('field_type_article')->isEmpty()) {

      $entity_names = [];
      foreach ($variables['node']->get('field_type_article')
                 ->referencedEntities() as $entity) {
        $entity_names[] = $entity->label();
      }
      $article_type = implode(', ', $entity_names);
    }
    $variables['content']['type_article'] = $article_type;

    // Navigation (contenus liés)
    $navigation = [];
    /** @var ArticleService $article_service */
    $article_service = \Drupal::service('dnb_core.service.article');
    $navigation_list = $article_service->getNavigation($variables['node']);

    foreach ($navigation_list as $id => $article) {
      $navigation[$id] = $article->getTitle();
    }
    $variables['content']['navigation'] = $navigation;

    // Tags
    $article_tags = '';
    if (!$variables['node']->get('field_tags')->isEmpty()) {

      $entity_names = [];
      foreach ($variables['node']->get('field_tags')
                 ->referencedEntities() as $entity) {
        $entity_names[] = $entity->link();
      }
      $article_tags = implode(', ', $entity_names);
    }
    $variables['content']['tags'] = $article_tags;

    // Dates
    $date_formatter = \Drupal::service('date.formatter');
    $variables['content']['date_creation'] = $date_formatter->format($variables['node']->getCreatedTime(), 'custom', 'd/m/Y');
    $variables['content']['date_modification'] = $date_formatter->format($variables['node']->getRevisionCreationTime(), 'custom', 'd/m/Y');
  }
}

/**
 * Implements THEME_preprocess_comment().
 */
function dnb_core_preprocess_comment(&$variables) {

  static $nb = 0;

  $variables['css_class'] = ($nb % 2 == 0) ? 'light' : '';

  $nb++;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function dnb_core_form_node_article_form_alter(&$form, FormState $form_state, $form_id) {

  if (array_key_exists('field_categorie', $form)) {

    // Récupération de la catégorie précisée dans l'URL
    $current_category = Drupal::request()->query->get('category', '');

    if (array_key_exists($current_category, $form['field_categorie']['widget']['#options'])
      && empty($form['field_categorie']['widget']['#default_value'])
    ) {
      // Utilisation de la catégorie comme valeur par défaut
      $form['field_categorie']['widget']['#default_value'] = [$current_category];
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function dnb_core_form_views_exposed_form_alter(&$form, FormState $form_state, $form_id) {

  if (array_key_exists('text', $form)) {
    // Modification du libellé du bouton submit pour la page de recherche
    $form['actions']['submit']['#value'] = t('Search');

    // Ajout d'une validation à la soumission du formulaire
    $form['#validate'][] = '_dnb_core_form_views_exposed_validate';
  }
}

/**
 * Fonction de validation du formulaire de recherche.
 */
function _dnb_core_form_views_exposed_validate(&$form, FormState $form_state) {

  // Suppression du message d'erreur à l'arrivée sur le formulaire de recherche
  if (\Drupal::request()->query->get('text', '<empty>') === '<empty>') {
    $form_state->clearErrors();
  }
}

/**
 * Fonction de callback de pre_render du block Taxonomy menu.
 *
 * @param array $build
 *   Render array du block juste avant de le rendre.
 *
 * @return array
 *   Le render array modifié
 */
function _dnb_core_block_taxonomy_menu_prerender(array $build) {

  // Ajout de variables personnalisées concernant l'utilisateur courant
  $current_user = Drupal::currentUser();
  $build['content']['menu']['#variables'] = [
    'is_anonymous' => $current_user->isAnonymous(),
    'user_name' => $current_user->getAccountName(),
    'search_text' => \Drupal::request()->query->get('text', ''),
    'enable_user_menu' => strpos(\Drupal::request()->getHost(), 'devnotebook.fr') === FALSE,
  ];

  return $build;
}
