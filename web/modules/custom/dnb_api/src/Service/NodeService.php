<?php

namespace Drupal\dnb_api\Service;

use \Drupal;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\node\NodeInterface;

/**
 * Service pour simplifier l'utilisation des nœuds.
 */
class NodeService {

  /**
   * Gestionnaire d'entités.
   *
   * @var EntityTypeManager
   */
  private $entityTypeManager;

  /**
   * Gestionnaire d'entités.
   *
   * @var EntityStorageInterface
   */
  private $nodeStorage;

  /**
   * Crée une nouvelle instance.
   *
   * @param EntityTypeManager $entity_type_manager
   *   Gestionnaire d'entités.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->nodeStorage = $this->entityTypeManager->getStorage('node');
  }

  /**
   * Retourne le nœud courant.
   *
   * @return Node
   *   Le nœud courant
   */
  public function getCurrentNode() {

    return Drupal::routeMatch()->getParameter('node');
  }

  /**
   * Retourne les nœuds liés au terme dont le tid est en argument.
   *
   * @param int $tid
   *   Tid du terme.
   * @param string $node_type
   *   Type de nœuds à retourner.
   * @param string $field_name
   *   Nom du champ liant le nœud au terme.
   * @param bool $count
   *   Si true, seul le nombre de nœuds sera retourné.
   *
   * @return array|int
   *   La liste des nœuds, ou leur nombre
   */
  public function getContentsByTerm($tid, $node_type, $field_name, $count = FALSE) {

    $results = $this->nodeStorage->getQuery()
      ->condition('type', $node_type)
      ->condition($field_name . '.target_id', $tid)
      ->condition('status', NodeInterface::PUBLISHED)
      ->sort('created', 'DESC')
      ->execute();

    if ($count) {
      $results = count($results);
    }
    else {
      $results = Node::loadMultiple($results);
    }

    return $results;
  }

}
