<?php

namespace Drupal\dnb_api\Service;

/**
 * Service pour simplifier l'utilisation des entités.
 */
class EntityService {

  /**
   * Retourne la liste des types d'entité du site sous forme de tableau associatif.
   * Les clés sont les noms des entités et les valeurs les noms de classe PHP correspondants.
   *
   * @return array
   *   La liste des types d'entité
   */
  public function getEntiyTypes() {
    $manager = \Drupal::entityTypeManager();
    $definitions = $manager->getDefinitions();
    $types = [];
    foreach ($definitions as $machine => $type) {
      $class = $type->getClass();
      $implements = class_implements($class);
      if (isset($implements['Drupal\Core\Entity\ContentEntityInterface'])) {
        $types[$machine] = $type;
      }
    }

    return $types;
  }

}
