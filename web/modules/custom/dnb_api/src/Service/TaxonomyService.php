<?php

namespace Drupal\dnb_api\Service;

use \Drupal;
use \Exception;
use \stdClass;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Service de gestion de la taxonomie.
 */
class TaxonomyService {

  /**
   * Gestionnaire d'entités.
   *
   * @var EntityTypeManager
   */
  private $entityTypeManager;

  /**
   * Crée une nouvelle instance.
   *
   * @param EntityTypeManager $entity_type_manager
   *   Gestionnaire d'entités.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Retourne la liste des vocabulaires.
   *
   * @return Vocabulary[]
   *    La liste des vocabulaires
   */
  public function getVocabularies() {
    $vid_list = $this->entityTypeManager->getStorage('taxonomy_vocabulary')
      ->getQuery()->execute();

    return Vocabulary::loadMultiple($vid_list);
  }

  /**
   * Retourne les termes du vocabulaire dont le nom technique est en argument.
   *
   * @param string $vocab_name
   *   Nom du vocabulaire.
   * @param bool $as_object
   *   Si les termes doivent être retournés sous forme de Term ou
   *   sous forme d'objets simplifiés.
   *
   * @return Term[]|array
   *   La liste des termes du vocabulaire (à plat)
   */
  public function getVocabularyTermList($vocab_name, $as_object = FALSE) {

    $term_list = $this->entityTypeManager->getStorage('taxonomy_term')
      ->loadTree($vocab_name, 0, NULL, $as_object);
    return $term_list;
  }

  /**
   * Retourne les termes du vocabulaire dont le nom technique est en argument.
   *
   * @param string $vocab_name
   *   Nom du vocabulaire.
   *
   * @return \stdClass
   *   L'arbre des termes du vocabulaire
   */
  public function getVocabularyTermTree($vocab_name) {

    $term_list = $this->getVocabularyTermList($vocab_name, FALSE);
    return $this->buildTermTree($term_list);
  }

  /**
   * Retourne la catégorie courante, à partir de l'URL ou du nœud courant.
   *
   * @param array $vocab_menu_list
   *   Liste des noms techniques des vocabulaires constituant le menu.
   * @param bool $as_term
   *   Si la catégorie doit être retournée sous forme de terme
   *   ou d'objet simplifié.
   *
   * @return Term|\stdClass
   *   Le terme de la catégorie courante,
   *   ou null s'il n'y a pas de terme courant
   */
  public function getCurrentCategory(array $vocab_menu_list = [], $as_term = FALSE) {

    $category = NULL;

    $current_term = Drupal::routeMatch()->getParameter('taxonomy_term');

    if (!empty($current_term) && in_array($current_term->getVocabularyId(), $vocab_menu_list)) {
      $category = $current_term;
    }
    else {

      $current_node = Drupal::routeMatch()->getParameter('node');
      if (empty($current_node)) {
        $current_node = Drupal::routeMatch()->getParameter('entity');
      }

      if (!empty($current_node) && $current_node->hasField('field_categorie')) {
        $current_category_tid = $current_node->get('field_categorie')
          ->getString();

        $category = !empty($current_category_tid) ? Term::load($current_category_tid) : NULL;
      }
    }

    if (!empty($category) && !$as_term) {
      $category = (object) [
        'tid' => $category->id(),
        'name' => $category->get('name')->getString(),
        'path' => Drupal::urlGenerator()
          ->generateFromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $category->id()]),
      ];
    }

    return $category;
  }

  /**
   * Construit un arbre de termes de taxonomie à partir de la liste en argument.
   *
   * @param array $term_list
   *   Liste de termes de taxonomie.
   * @param bool $as_term
   *   Si les termes doivent être retournés sous forme de termes
   *   ou d'objets simplifiés.
   *
   * @return \stdClass
   *   L'arbre des termes, sous forme d'objets aux propriétés suivantes :
   *      - term (Term|\stdClass)
   *      - children (Tableau d'objets de ce type)
   *      - ancestor_id_list (Liste des id des ancêtres)
   */
  public function buildTermTree(array $term_list, $as_term = FALSE) {

    $tree = [];
    $children = [];
    $ancestors = [];

    $route_generator = Drupal::urlGenerator();

    // Création d'une structure exploitable
    $term_object_list = [];

    foreach ($term_list as $term) {

      if (!$as_term) {
        $term_object = (object) [
          'tid' => $term->tid,
          'name' => $term->name,
          'path' => $route_generator->generateFromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $term->tid]),
        ];
      }
      else {
        $term_object = Term::load($term->tid);
      }

      $term_object_list[] = (object) [
        'id' => $term->tid,
        'parent_id' => $term->parents[0],
        'term' => $term_object,
      ];
    }

    // Mémorisation des liens entre les emplacements
    foreach ($term_object_list as $term_std) {
      $children[$term_std->parent_id][] = $term_std;
      if ($term_std->parent_id > 0) {
        $ancestors[$term_std->id][] = $term_std->parent_id;
        if (array_key_exists($term_std->parent_id, $ancestors)) {
          $ancestors[$term_std->id] = array_merge($ancestors[$term_std->id], $ancestors[$term_std->parent_id]);
        }
      }
    }

    // Création de l'arbre
    foreach ($term_object_list as $term_std) {

      if (isset($children[$term_std->id])) {
        $term_std->children = $children[$term_std->id];
      }
      else {
        $term_std->children = [];
      }
      if (isset($ancestors[$term_std->id])) {
        $term_std->ancestor_id_list = $ancestors[$term_std->id];
      }
      else {
        $term_std->ancestor_id_list = [];
      }

      unset($term_std->id);
      unset($term_std->parent_id);
    }

    if (!empty($children)) {

      $tree = array_shift($children);

      if (is_array($tree)) {

        // L'arborescence n'a pas de noeud racine.
        // Elle est constituée d'une liste d'arbre
        // On crée une racine.
        $tree = (object) [
          'term' => NULL,
          'children' => $tree,
          'ancestor_id_list' => [],
        ];
      }
      else {
        $tree = $children;
      }
    }

    return $tree;
  }

  /**
   * Retourne le sous-arbre dont la racine est le tid en argument.
   *
   * @param array $tree
   *   Arbre référence.
   * @param int $tid
   *   Tid de la racine du sous-arbre recherché.
   *
   * @return \stdClass|null
   *   Le sous-arbre si la racine a été trouvée
   */
  public function getSubTree(array $tree, $tid) {

    $subtree = NULL;

    if (!empty($tree)) {
      $i = 0;
      while (empty($subtree) && $i < count($tree)) {
        if ($tree[$i]->term->tid == $tid) {
          $subtree = $tree[$i];
        }
        else {
          if (!empty($tree[$i]->children)) {
            $subtree = $this->getSubTree($tree[$i]->children, $tid);
          }
        }
        $i++;
      }
    }

    return $subtree;
  }

  /**
   * Retourne le sous-arbre parent.
   *
   * Le sous-arbre retourné aura pour racine le parent du terme
   * dont le tid est en argument.
   *
   * @param \stdClass $tree
   *   Arbre référence.
   * @param int $tid
   *   Tid du terme dont le parent est la racine du sous-arbre recherché.
   * @param \stdClass|null $parent_tree
   *   Sous-arbre parent, ou null si on est à la première récursion.
   *
   * @return \stdClass|null
   *   Le sous-arbre si la racine a été trouvée
   */
  public function getParentSubTree(stdClass $tree, $tid, $parent_tree = NULL) {
    $subtree = NULL;

    if (!empty($tree->children)) {

      // Niveau 0 (ex: Web, Java, ...)
      if (empty($parent_tree)) {
        // On génère un parent
        $parent_tree = (object) [
          'term' => [
            'path' => '/',
            'name' => t('Home'),
          ],
          'children' => $tree->children,
        ];
      }

      foreach ($tree->children as $child) {

        // Si le noeud est celui en argument
        if ($child->term->tid == $tid || $tid == 0) {
          $subtree = $parent_tree;
          break;
        }
        elseif (!empty($child->children)) {
          $subtree = $this->getParentSubTree($child, $tid, $child);
          if (!empty($subtree)) {
            break;
          }
        }
      }
    }

    return $subtree;
  }

  /**
   * Retourne les tid des ancêtres du terme dont le tid est en argument.
   *
   * @param int $term_id
   *   ID du terme dont on veut les ancêtres.
   *
   * @return array
   *   La liste des tid des ancêtres, du plus proche au plus haut dans
   *   l'arborescence
   *
   * @throws Exception
   *   Si le tid en argument n'existe pas.
   */
  public function getAncestorTids($term_id) {
    static $hierarchy = [];
    $ancestor_tids = [];

    if (empty($hierarchy)) {
      $database = Drupal::database();
      $hierarchy = $database->select('taxonomy_term_hierarchy', 'tth')
        ->fields('tth')
        ->execute()
        ->fetchAllKeyed();
    }

    if (!array_key_exists($term_id, $hierarchy)) {
      throw new Exception('Terme inconnu ' . $term_id . '.');
    }

    $current_tid = $term_id;
    while ($current_tid != 0) {

      $parent_tid = $hierarchy[$current_tid];
      $current_tid = $parent_tid;
      if (!empty($parent_tid)) {
        $ancestor_tids[] = $parent_tid;
      }
    }

    return $ancestor_tids;
  }

  /**
   * Retourne les tid des descendants du terme dont le tid est en argument.
   *
   * @param int $term_id
   *   ID du terme dont on veut les descendants.
   *
   * @return array
   *   La liste des tid des descendants, du plus proche au plus bas dans
   *   l'arborescence
   *
   * @throws Exception
   *   Si le tid en argument n'existe pas.
   */
  public function getChildrenTids($term_id) {
    static $hierarchy = [];

    if (empty($hierarchy)) {
      $database = Drupal::database();
      $hierarchy = $database->select('taxonomy_term_hierarchy', 'tth')
        ->fields('tth')
        ->execute()
        ->fetchAllKeyed();
    }

    if (!array_key_exists($term_id, $hierarchy)) {
      throw new Exception('Terme inconnu ' . $term_id . '.');
    }

    $descendant_tids = array_keys($hierarchy, $term_id);

    return $descendant_tids;
  }

}
