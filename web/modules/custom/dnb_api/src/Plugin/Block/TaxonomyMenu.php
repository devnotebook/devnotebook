<?php

namespace Drupal\dnb_api\Plugin\Block;

use \Drupal;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dnb_api\Service\TaxonomyService;
/**
 * Bloc pour afficher un arbre de catégories sous forme de menu.
 *
 * @Block(
 * id = "dnb_taxonomy_menu",
 * admin_label = @Translation("Taxonomy menu block"),
 * category = @Translation("Menus")
 * )
 */
class TaxonomyMenu extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();
    $vocabulary_vid = isset($config['vocabulary']) ? $config['vocabulary'] : NULL;

    $term_tree = [];
    $current_category = NULL;
    $ancestors = [];

    if (!empty($vocabulary_vid)) {
      /** @var TaxonomyService $taxonomy_service */
      $taxonomy_service = Drupal::service('dnb_api.service.taxonomy');

      // Récupération de la catégorie courante
      $current_category = $taxonomy_service->getCurrentCategory([$vocabulary_vid]);
      $ancestors = [];
      if (!empty($current_category)) {
        $ancestors = $taxonomy_service->getAncestorTids($current_category->tid);
      }

      // Récupération des catégories à afficher
      $term_tree = $taxonomy_service->getVocabularyTermTree($vocabulary_vid);
    }

    $build = [
      '#cache' => [
        'tags' => ['taxonomy_term_list'],
        'contexts' => ['user'],
        'max-age' => Cache::PERMANENT,
      ],
      'menu' => [
        '#theme' => 'taxonomy_menu',
        '#tree' => $term_tree,
        '#current_term' => $current_category,
        '#current_term_ancestors' => $ancestors,
        '#variables' => [],
      ],
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    /** @var TaxonomyService $taxonomy_service */
    $taxonomy_service = Drupal::service('dnb_api.service.taxonomy');

    $options = [];
    foreach ($taxonomy_service->getVocabularies() as $vid => $vocabulary) {
      $options[$vid] = $vocabulary->label();
    }

    $form['vocabulary'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => $this->t('Vocabulary constituting the menu'),
      '#default_value' => isset($config['vocabulary']) ? $config['vocabulary'] : NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['vocabulary'] = $form_state->getValue(
      'vocabulary'
    );
  }

}