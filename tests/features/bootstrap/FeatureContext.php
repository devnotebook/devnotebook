<?php

use Drupal\DrupalExtension\Context\RawDrupalContext;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Tester\Exception\PendingException;
use Drupal\taxonomy\Entity\Term;
use Drupal\block\Entity\Block;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawDrupalContext implements SnippetAcceptingContext {

  private $currentCategory = NULL;
  private $blockView = NULL;

  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   */
  public function __construct() {
  }

  /**
   * @Given /^la catégorie courante : "([^"]*)"$/
   */
  public function categorieCourante($category_id) {

    if ($category_id == 'null') {
      $this->currentCategory = Term::load($category_id);
    }
  }

  /**
   * @When /^le menu de catégories s'affiche$/
   */
  public function affichageMenu() {

    $block = Block::load('dnb_navigation');
    $view_builder = Drupal::entityTypeManager()->getViewBuilder('block');
    $block_view = $view_builder->view($block);
    $block_view['current_category'] = $this->currentCategory;
    $this->blockView = $block_view;
  }

  /**
   * @Then /^le menu de catégories doit être vide$/
   */
  public function menuVide() {
    PHPUnit_Framework_Assert::assertEmpty($this->blockView['tree']);
  }

  /**
   * @Then /^il a pour titre "([^"]*)"$/
   */
  public function aPourTitre($title) {
    throw new PendingException();
  }

  /**
   * @Then /^la catégorie "([^"]*)" est mise en valeur$/
   */
  public function categorieMiseEnValeur($category) {
    throw new PendingException();
  }

  /**
   * @Then /^les catégories racines sont affichées$/
   */
  public function categorieRacinesAffichees() {
    throw new PendingException();
  }

  /**
   * @Then /^les catégories filles de "([^"]*)" sont déroulées$/
   */
  public function categorieFillesDeroulees($category) {
    throw new PendingException();
  }
}
