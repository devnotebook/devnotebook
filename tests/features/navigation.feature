# language: fr
  Fonctionnalité: Menu de catégories
    Selon la page sur laquelle je suis, le menu de catégories est différent.
    Le titre du bloc et les catégories qu'il contient changent en fonction de la page courante.

  Scénario: Page d'accueil
    Etant donné la catégorie courante : "null"
    Quand le menu de catégories s'affiche
    Alors le menu de catégories doit être vide

    Scénario: Pages de rubrique de niveau 1
      Etant donné la catégorie courante : "4"
      Quand le menu de catégories s'affiche
      Alors il a pour titre "Accueil"
      Et les catégories racines sont affichées
      Et la catégorie "Web" est mise en valeur
      Et les catégories filles de "Web" sont déroulées

    Plan du scénario: Pages de rubrique des niveaux inférieurs
      Etant donné la catégorie courante : "<page>"
      Quand le menu de catégories s'affiche
      Alors il a pour titre "<titre>"
      Et la catégorie "<enValeur>" est mise en valeur
      Et les catégories filles de "<titre>" sont déroulées

      Exemples:
      | page     | titre                | enValeur |
      | 9        | Web                  | Drupal   |
      | 16       | Systèmes et réseaux  | Windows  |


