#!/usr/bin/env bash

# Options d'exécution de drush
DRUSH="$(pwd)/vendor/bin/drush -r $(pwd)/web"

# Configuration
. "$(pwd)/settings/drush-config.sh"

# Préparation des répertoires
if [ ! -d web/sites/default/files ]
  then
    mkdir -m775 web/sites/default/files
fi

# Préparation des fichiers de configuration (settings.php)
if [ -f web/sites/default/settings.php ]
  then
    chmod +w "$(pwd)/web/sites/default"
    chmod +w "$(pwd)/web/sites/default/settings.php"
    rm "$(pwd)/web/sites/default/settings.php"
fi
cp "$(pwd)/settings/settings.php" "$(pwd)/web/sites/default/settings.php"

# Préparation des fichiers de configuration (services.yml)
if [ -f web/sites/default/services.yml ]
  then
    chmod +w "$(pwd)/web/sites/default"
    chmod +w "$(pwd)/web/sites/default/services.yml"
    rm "$(pwd)/web/sites/default/services.yml"
fi
cp "$(pwd)/settings/services.yml" "$(pwd)/web/sites/default/services.yml"

# Préparation des fichiers de configuration (settings.local.php)
if [ ! -f web/sites/default/settings.local.php ]
  then
    ln -s "$(pwd)/settings/settings.local.php" "$(pwd)/web/sites/default/settings.local.php"
fi

# Préparation des fichiers de configuration (development.services.yml)
if [ -f settings/development.services.yml ]
  then
    rm -f "$(pwd)/web/sites/development.services.yml"
    ln -s "$(pwd)/settings/development.services.yml" "$(pwd)/web/sites/development.services.yml"
fi

# Installation du site
$DRUSH site-install -y \
  --account-name=$DRUSH_INSTALL_ACCOUNT_NAME \
  --account-pass=$DRUSH_INSTALL_ACCOUNT_PASS \
  --account-mail=$DRUSH_INSTALL_ACCOUNT_MAIL \
  --locale=$DRUSH_INSTALL_LOCALE \
  $DRUSH_INSTALL_PROFILE

# Reconstruction des caches
$DRUSH cr

# Mise à jour de l'UUID du site (à commenter à la première installation du site en dev)
$DRUSH cset system.site uuid "1501bbbe-dd1c-46ec-addf-50e3c97b1740" -y

# Mise à jour de l'UUID de langue (à commenter à la première installation du site en dev)
$DRUSH cset language.entity.fr uuid "a9bee0a3-74cd-42ee-8006-24c8f53f12e7" -y
$DRUSH cset language.entity.und uuid "72613821-facf-438f-8504-a3c9844422e3" -y
$DRUSH cset language.entity.zxx uuid "ab9aa992-13a4-49cf-8656-d4a375192917" -y

# Import de la configuration du site
$DRUSH cim --quiet -y

# Réinstallation des modules de développement
if [ ! -z "$DRUSH_DEV_ONLY_MODULES" ]
  then
    $DRUSH en $DRUSH_DEV_ONLY_MODULES -y
fi

# Import des contenus du site
. "$(pwd)/scripts/import-data.sh"

# Ajout des droits d'écriture pour la configuration locale
chmod +w "$(pwd)/web/sites/default/settings.local.php"
