#!/usr/bin/env bash

# Activation des modules d'import/export
$DRUSH en dnb_importexport -y

# Import des termes de taxonomie
$DRUSH migrate-status
$DRUSH migrate-import term_type_article
$DRUSH migrate-import term_categorie
$DRUSH migrate-status

# Désactivation des modules d'import/export
$DRUSH pmu dnb_importexport -y
$DRUSH pmu migrate_source_csv -y
$DRUSH pmu migrate_tools -y
$DRUSH pmu migrate_plus -y
$DRUSH pmu migrate_drupal -y
$DRUSH pmu migrate -y