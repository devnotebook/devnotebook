#!/usr/bin/env bash

# Exécutable behat
BEHAT="$(pwd)/vendor/bin/behat"
TESTS_DIR="tests"

cd $TESTS_DIR
echo $BEHAT "$@"

$BEHAT "$@"
