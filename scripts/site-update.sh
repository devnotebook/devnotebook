#!/usr/bin/env bash

# Options d'exécution de drush
DRUSH="$(pwd)/vendor/bin/drush -r $(pwd)/web"

# Configuration
. "$(pwd)/settings/drush-config.sh"

# Préparation des répertoires
if [ ! -d web/sites/default/files ]
  then
    mkdir -m775 web/sites/default/files
fi

# Préparation des fichiers de configuration (settings.php)
if [ -f web/sites/default/settings.php ]
  then
    chmod +w "$(pwd)/web/sites/default"
    chmod +w "$(pwd)/web/sites/default/settings.php"
    rm "$(pwd)/web/sites/default/settings.php"
fi
cp "$(pwd)/settings/settings.php" "$(pwd)/web/sites/default/settings.php"

# Préparation des fichiers de configuration (services.yml)
if [ -f web/sites/default/services.yml ]
  then
    chmod +w "$(pwd)/web/sites/default"
    chmod +w "$(pwd)/web/sites/default/services.yml"
    rm "$(pwd)/web/sites/default/services.yml"
fi
cp "$(pwd)/settings/services.yml" "$(pwd)/web/sites/default/services.yml"

# Préparation des fichiers de configuration (settings.local.php)
if [ -f web/sites/default/settings.local.php ]
  then
    rm "$(pwd)/web/sites/default/settings.local.php"
fi
ln -s "$(pwd)/settings/settings.local.php" "$(pwd)/web/sites/default/settings.local.php"

# Préparation des fichiers de configuration (development.services.yml)
if [ -f settings/development.services.yml ]
  then
    rm -f "$(pwd)/web/sites/development.services.yml"
fi
ln -s "$(pwd)/settings/development.services.yml" "$(pwd)/web/sites/development.services.yml"

# Mise en maintenance du site
$DRUSH state-set system.maintenance_mode 1

# Désinstallation des modules de développement
if [ ! -z "$DRUSH_DEV_ONLY_MODULES" ]
  then
    $DRUSH pmu $DRUSH_DEV_ONLY_MODULES -y
fi

# Effectue les mise à jour en base des modules
$DRUSH updb -y

# Import de la configuration du site
$DRUSH cim sync -y

# Mise à jour du schéma des entités
$DRUSH entup -y

# Réinstallation des modules de développement
if [ ! -z "$DRUSH_DEV_ONLY_MODULES" ]
  then
    $DRUSH en $DRUSH_DEV_ONLY_MODULES -y
fi

# Mise en ligne du site
$DRUSH state-set system.maintenance_mode 0

# Reconstruction des caches
$DRUSH cr

# Ajout des droits d'écriture pour la configuration locale
chmod +w "$(pwd)/web/sites/default/settings.local.php"
