#!/usr/bin/env bash

# Options d'exécution de drush
DRUSH="$(pwd)/vendor/bin/drush -r $(pwd)/web"

# Configuration
. "$(pwd)/settings/drush-config.sh"

# Désinstallation des modules de développement
if [ ! -z "$DRUSH_DEV_ONLY_MODULES" ]
  then
    $DRUSH pmu $DRUSH_DEV_ONLY_MODULES -y
fi

# Export de la configuration du site
$DRUSH cex -y

# Réinstallation des modules de développement
if [ ! -z "$DRUSH_DEV_ONLY_MODULES" ]
  then
    $DRUSH en $DRUSH_DEV_ONLY_MODULES -y
fi
