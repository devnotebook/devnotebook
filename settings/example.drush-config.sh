#!/usr/bin/env bash

export DRUSH_INSTALL_ACCOUNT_NAME="admin"
export DRUSH_INSTALL_ACCOUNT_PASS="admin"
export DRUSH_INSTALL_ACCOUNT_MAIL="v.nivuahc@devnotebook.fr"
export DRUSH_INSTALL_LOCALE="fr"
export DRUSH_INSTALL_PROFILE="devnotebook"

export DRUSH_INSTALL_MODULES="devel kint"