#!/usr/bin/env bash

# Paramètres d'installation
export DRUSH_INSTALL_ACCOUNT_NAME="admin"
export DRUSH_INSTALL_ACCOUNT_PASS="admin"
export DRUSH_INSTALL_ACCOUNT_MAIL="v.nivuahc@ntymail.com"
export DRUSH_INSTALL_LOCALE="fr"
export DRUSH_INSTALL_PROFILE="devnotebookp"

# Modules à installer uniquement en développement
export DRUSH_DEV_ONLY_MODULES="devel kint"